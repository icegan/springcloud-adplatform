package cn.dota2info.elk.Service;


import cn.dota2info.elk.dao.ElkDocDao;
import cn.dota2info.elk.dao.ElkSearchDao;
import cn.dota2info.elk.entity.BaseSearchParam;
import cn.dota2info.elk.entity.Book;
import com.wwmxd.common.msg.ListRestResponse;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
@Slf4j
public class BookService {
    @Autowired
    private ElkDocDao elkDao;
    @Autowired
    private ElkSearchDao searchDao;

    public String insert(Book book){
        return elkDao.insertObject(book,"book","doc",null).getId();
    }

    public Book get(Integer id){
        return (Book) elkDao.getObject("book","doc",Integer.toString(id),Book.class);
    }
    public boolean delete(String id){
        return  elkDao.deleteObject("book","doc",id);
    }
    public Long update(Book book){
        return elkDao.updateObject(book,"book","doc").getVersion();
    }
    public ListRestResponse getAllBook(BaseSearchParam param) throws Exception {
       ListRestResponse list=new ListRestResponse();
       log.info(param.toString());
       if(param.getSearchValue()==null||"".equals(param.getSearchValue())){
           list.setResult( elkDao.getAll("book","doc",Book.class,param));
       }else {
           list.setResult(searchDao.termQuery("book","doc",param, Book.class));
       }

       list.setCount(Math.toIntExact(getCount(param)));
        return list;
    }
    public Long getCount(BaseSearchParam param) throws Exception {
        return elkDao.getCount("book","doc",Book.class,param);
    }
}
